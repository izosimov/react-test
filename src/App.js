import React from 'react';
import BackgroundContent from './components/BackgroundContent/BackgroundContent.jsx';
import WordsList from './components/WordsList/WordsList.jsx';
import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_AUTHDOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MESSUREMENT_ID
};

firebase.initializeApp(firebaseConfig);
const databse = firebase.database();

class App extends React.Component {
    state = {
        wordsList: []
    };

    componentDidMount() {
        databse.ref('/cards/').once('value').then(res => {
            this.setState(() => ({
                wordsList: res.val()
            }));
        });
    }

    handleDelete = cardIndex => {
        databse.ref(`/cards/${cardIndex}`).remove();

        databse.ref('/cards/').once('value').then(res => {
            this.setState(() => ({
                wordsList: res.val()
            }));
        });
    };

    handleCreating = (eng, rus) => {
        const card = {
            eng,
            rus,
            id: Math.floor(Math.random() * Math.floor(100))
        };

        databse.ref('/cards/' + this.state.wordsList.length).set(card);

        this.setState(prevState => ({
            wordsList: [...prevState.wordsList, card]
        }));
    };

    render() {
        const { wordsList } = this.state;

        return (
            <>
                <BackgroundContent>
                    <h1>Карточки со словами</h1>
                </BackgroundContent>

                <BackgroundContent isHideBG>
                    <WordsList
                        data={wordsList}
                        onDelete={this.handleDelete}
                        onCreate={this.handleCreating}
                    />
                </BackgroundContent>
            </>
        );
    }
}

export default App;
