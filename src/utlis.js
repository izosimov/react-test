export const generateLastIndex = arr => {
	let currentLastIndex = 0;

	arr.forEach(el => {
		if (el.id > currentLastIndex)  currentLastIndex = el.id;
	});

	return currentLastIndex + 1;
};