const API_KEY = process.env.REACT_APP_YANDEX_API_KEY;

export const getTranslatedWord = async (text = '', lang = 'ru-en') => {
	const res = await fetch(
		`https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${API_KEY}&lang=${lang}&text=${text}`
	);

	const body = await res.json();

	return body.def || [];
};

export default {
	getTranslatedWord
}