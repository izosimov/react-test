import React, { Component } from 'react';
import { CheckCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import s from './Card.module.scss';
import cl from 'classnames';

class Card extends Component {
	state = {
		isClicked: false,
		isRemembered: false
	};

	handleClick = () => {
		this.setState(prevSate => {
			return ({
				isClicked: !prevSate.isClicked
			})
		})
	};

	handleRemember = () => {
		this.setState(prevSate => {
			return ({
				isRemembered: !prevSate.isRemembered
			})
		})
	};

	handleDelete = () => {
		const { onDelete } = this.props;

		onDelete();
	};

	render() {
		const { eng, rus } = this.props;
		const { isClicked, isRemembered } = this.state;

		const cardClasses = cl(s.card, s['mr-12'], { [s.done]: isClicked, [s.remembered]: isRemembered });

		return (
			<div className={s.root}>
				<div
					className={cardClasses}
					onClick={this.handleClick}
				>
					<div className={s.cardInner}>
						<div className={s.cardFront}>
							{ eng }
						</div>
						<div className={s.cardBack}>
							{ rus }
						</div>
					</div>
				</div>

				<button className={cl(s.btn, s['mr-12'])} onClick={this.handleRemember}>
					<CheckCircleOutlined />
				</button>

				<button className={s.btn} onClick={this.handleDelete}>
					<DeleteOutlined />
				</button>
			</div>
		);
	}
}

export default Card;