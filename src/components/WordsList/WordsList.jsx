import React, { Component } from 'react';
import s from './WordsList.module.scss';
import cl from 'classnames';
import Card from './Card/Card.jsx';

import { getTranslatedWord } from '../../services/yandex-dictionary.js';

import { Input, Button, notification } from 'antd';

class WordsList extends Component {

	state = {
		newCard: {
			eng: '',
			rus: ''
		}
	};

	handleSubmitCreatingCard = evt => {
		evt.preventDefault();

		const { onCreate } = this.props;
		const { eng, rus } = this.state.newCard;

		if (eng && rus) onCreate(eng, rus);

		this.resetNewCardFields();
	};

	handleInputRusWord = evt => {
		if (!evt.target) return;

		this.setState(prevState => ({
			newCard: {
				...prevState.newCard,
				rus: evt.target ? evt.target.value : prevState.newCard.rus
			}
		}));
	};

	handleSearchRusWord = async value => {
		if (!value) {
			notification.warning({ message: 'Заполните поле ввода' });
			return;
		}

		const translatedWordOptions = await getTranslatedWord(value);

		if (translatedWordOptions.length) {
			this.setState(() => ({
				newCard: {
					rus: value,
					eng: translatedWordOptions[0].tr[0].text
				}
			}));
		} else {
			notification.error({ message: 'Перевод не найден' });
		}
	};

	resetNewCardFields = () => {
		this.setState(() => ({
			newCard: {
				eng: '',
				rus: ''
			}
		}));
	};

	render() {
		const { data, onDelete } = this.props;

		const cards = data.map((card, index) => (
			<Card
				eng={card.eng}
				rus={card.rus}
				key={card.id}
				onDelete={() => onDelete(index)}
			/>
		));

		return (
			<>
				<section>
					<div
						className={cl(s.newCard, s.mb24)}
					>
						<h3 className={s.mb12}>Добавить новое слово</h3>

						<Input.Search
							className={cl(s.mb24)}
							placeholder="Введите русское слово"
							enterButton="Search"
							size="medium"
							onChange={this.handleInputRusWord}
							onSearch={this.handleSearchRusWord}
						/>

						<p className={cl(s.mb24)}>
							{this.state.newCard.rus} - {this.state.newCard.eng}
						</p>

						<Button
							type="primary"
							onClick={this.handleSubmitCreatingCard}
						>
							Добавить карточку
						</Button>
					</div>
				</section>

				<section className={s['cards-list']}>
					{ cards }
				</section>
			</>
		);
	}
}

export default WordsList;