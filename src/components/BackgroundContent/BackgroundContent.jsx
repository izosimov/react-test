import React from 'react';
import s from './BackgroundContent.module.scss';

function App({ isHideBG, children }) {
	const mainClasses = [s.cover];
	if (isHideBG) mainClasses.push(s.img);

	return (
		<div className={mainClasses.join(' ')}>
			<div className={s.wrap}>
				{ children }
			</div>
		</div>
	);
}

export default App;
